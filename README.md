# Bad_UI


### Installation
1. Download or clone this repository 
    * If downloaded unzip the package.
2. Open the main folder and drag or open the main.html file with your browser
3. Try to make it to the end where the credits are shown
    * Tip: The second page, the form, is very hard to beat. If you want to skip it you could continue with the login.html file in the login folder



### Description
As one of our initial tasks at CODERS.BAY Vienna, we were tasked with creating a website in 2 weeks using HTML5, CSS3, and JavaScript, designed to make user interaction as challenging as possible. This project allowed us to freely explore working with vanilla JavaScript and express our creativity in a unique way.

